#!/bin/bash 
menu(){
	echo "digite a para selecionar um arquivo"
	echo "digite b para fazer um preview do arquivo"
	echo "digite c para exibir o arquivo selecionado"
	echo "digite d para criar um arquivo com n linhas blablabla"
	echo "digite e para pesquisar algo no arquivo selecionado"
	echo "digite f para criar uma copia do arquivo selecionado"
	echo "digite g para sair"
}
while [ "$op" != "g" ]; do
	echo -e "selecione uma opção"
	menu
	read op
	if [ $op = "a" ]; then
		read arq
	fi
	if [ $op = "b" ]; then
		head -2 $arq
		sleep 1
		echo "..."
		sleep 1
		tail -2 $arq
	fi
	if [ $op = "c" ]; then
		cat $arq
	fi
	if [ $op = "d" ]; then
		echo "passe a quantidade de linhas: "
		read linhas
		./blablabla.sh $linhas > arquivo.txt
	fi
	if [ $op = "e" ]; then
		echo "digite a pesquisa: "
		read pesq
		cat $arq | grep $pesq
	fi
	if [ $op = "f" ]; then
		cp $arq copia.txt
	fi
done
